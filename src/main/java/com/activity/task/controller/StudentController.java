package com.activity.task.controller;

import com.activity.task.entity.Student;
import com.activity.task.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/school")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @PostMapping("/addstudent")
    public Student addStudents (Student student){
        return studentRepository.save(student);
    }

    @GetMapping("/students")
    public @ResponseBody Iterable<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    @GetMapping("/student")
    public ResponseEntity<List<Student>> getById (@RequestParam int student_id){
        return new ResponseEntity<>(studentRepository.findBystudentId(student_id), HttpStatus.OK);
    }
}
